const helper = require("../helper/");
const multer = require("multer");
const fileUpload = require("../helper/fileUpload");

const upload = fileUpload.single("profile_picture");

const uploadFilter = (req, res, next) => {
  upload(req, res, (err) => {
    if (err instanceof multer.MulterError) {
      console.log(err);
      return helper.response(res, 400, { message: "File Tidak Cocok" });
    } else if (err) {
      console.log(err);
      return helper.response(res, 400, { message: "File Tidak Cocok" });
    }
    next();
  });
};

module.exports = {
  uploadFilter,
};
