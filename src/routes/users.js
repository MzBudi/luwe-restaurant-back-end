const express = require("express");
const Route = express.Router();
const {
  getUser,
  deleteUser,
  updateUser,
  changePassword,
  changeProfile,
  getProfile,
} = require("../controller/users");
const { authorization } = require("../middleware/authentication");
const { uploadFilter } = require("../middleware/uploader");

Route.get("/", authorization, getUser)
  .get("/profile/:user_id", authorization, getProfile)
  .delete("/:user_id", authorization, deleteUser)
  .put("/:user_id", authorization, updateUser)
  .put("/changePassword/:user_id", authorization, changePassword)
  .put("/changeProfile/:user_id", uploadFilter, authorization, changeProfile);

module.exports = Route;
