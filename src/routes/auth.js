const express = require("express");
const Route = express.Router();
const { userLogin, createUser } = require("../controller/auth");
const { uploadFilter } = require("../middleware/uploader");

Route
    .post("/login", userLogin)
    .post("/register", uploadFilter, createUser);

module.exports = Route;
